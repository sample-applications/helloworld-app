/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <gtk/gtk.h>
#include "helloworld-app.h"
#include "helloworld-app-window.h"

#define WIN_WIDTH  480
#define WIN_HEIGHT 800

struct _HlwAppWindow
{
  GtkApplicationWindow parent;
  GtkWidget *button, *button_box;
};

G_DEFINE_TYPE(HlwAppWindow, hlw_app_window, GTK_TYPE_APPLICATION_WINDOW);

static void
hlw_app_window_init (HlwAppWindow *self)
{
 HlwAppWindow *win = HLW_APP_WINDOW(self);
 gtk_window_set_default_size(GTK_WINDOW(win),WIN_WIDTH,WIN_HEIGHT);
 win->button = gtk_button_new_with_label("Hello World!");
 win->button_box = gtk_button_box_new (GTK_ORIENTATION_HORIZONTAL);
 gtk_container_add (GTK_CONTAINER(win->button_box), win->button);
 gtk_container_add (GTK_CONTAINER(win), GTK_WIDGET(win->button_box));
 gtk_widget_show_all(GTK_WIDGET(win));
}

static void
hlw_app_window_class_init (HlwAppWindowClass *klass)
{
}

HlwAppWindow *
hlw_app_window_new (HlwApp *app)
{
  return g_object_new (HLW_APP_WINDOW_TYPE, "application", app, NULL);
}
