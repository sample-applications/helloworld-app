/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <config.h>
#include "helloworld-app.h"
#include "helloworld-app-window.h"
#include <gtk/gtk.h>

struct _HlwApp
{
  GtkApplication parent;
};

G_DEFINE_TYPE(HlwApp, hlw_app, GTK_TYPE_APPLICATION);

static void
hlw_app_activate (GApplication *app)
{
  HlwAppWindow *win;
  win = hlw_app_window_new (HLW_APP (app));
  gtk_window_present (GTK_WINDOW (win));
}


static void
hlw_app_class_init (HlwAppClass *class)
{
  G_APPLICATION_CLASS (class)->activate = hlw_app_activate;
}

static void
hlw_app_init (HlwApp *self)
{
}

HlwApp *
hlw_app_new (void)
{
  return g_object_new (HLW_TYPE_APP,
                       "application-id", "org.apertis.HelloWorld.App",
                       NULL);
}
